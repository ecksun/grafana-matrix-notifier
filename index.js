const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const markdown = require('markdown').markdown;

const Matrix = require('./matrix');

const app = new Koa();
const router = new Router();

const matrix = new Matrix();

router.post('/grafana', (ctx, next) => {
    // TODO Add prometheus counters on requests
    console.log(ctx.request.body);

    const body = ctx.request.body;

    // TODO Move markdown generation to a separate thing
    const md = `
# ${body.title} - ${body.ruleName}
${body.message} ([more info](${body.ruleUrl}))
    `;
    const html = markdown.toHTML(md.trim());
    matrix.send(md, html);

    ctx.status = 200;
    next();
});

app
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);
