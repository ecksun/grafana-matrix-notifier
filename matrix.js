const { MatrixClient, SimpleFsStorageProvider, RichReply } = require('matrix-bot-sdk');

// where you would point a client to talk to a homeserver
// TODO read from env variable
const homeserverUrl = "http://localhost:8008";

// see https://t2bot.io/docs/access_tokens
// TODO Read from env variable
const accessToken = "MDAxN2xvY2F0aW9uIGxvY2FsaG9zdAowMDEzaWRlbnRpZmllciBrZXkKMDAxMGNpZCBnZW4gPSAxCjAwMjFjaWQgdXNlcl9pZCA9IEBib3Q6bG9jYWxob3N0CjAwMTZjaWQgdHlwZSA9IGFjY2VzcwowMDIxY2lkIG5vbmNlID0gXlJVQFc1WUkjeTU9Y3picwowMDJmc2lnbmF0dXJlIDLbX18csV0p67ZFMcttX-1lye2TpzVh-daF1Csv9XMPCg";

class Matrix {
    constructor() {
        // We'll want to make sure the bot doesn't have to do an initial sync every
        // time it restarts, so we need to prepare a storage provider. Here we use
        // a simple JSON database.
        // TODO Read from env variable
        const storage = new SimpleFsStorageProvider("hello-bot.json");

        // Now we can create the client and set it up to automatically join rooms.
        this.client = new MatrixClient(homeserverUrl, accessToken, storage);

        // this.client.on("room.message", handleCommand);

        this.client.start().then(() => console.log("Client started!"));
    }

    send(md, html) {
        // TODO Handle promise success/failure
        // TODO Prometheus counters on success/failure
        // TODO Read channel from env var
        this.client.sendMessage('!PEbGghMmMYbKyuVjty:localhost', {
            msgtype: 'm.text',
            body: md,
            formatted_body: html,
            format: 'org.matrix.custom.html',
        });
    }
}

module.exports = Matrix;

// TODO Remove
async function handleCommand(roomId, event) {
    console.log(roomId);
    console.log(event);
    // Don't handle events that don't have contents (they were probably redacted)
    if (!event["content"]) return;

    // Don't handle non-text events
    if (event["content"]["msgtype"] !== "m.text") return;

    // We never send `m.text` messages so this isn't required, however this is
    // how you would filter out events sent by the bot itself.
    if (event["sender"] === await client.getUserId()) return;

    // Make sure that the event looks like a command we're expecting
    const body = event["content"]["body"];
    if (!body || !body.startsWith("!hello")) return;

    // If we've reached this point, we can safely execute the command. We'll
    // send a reply to the user's command saying "Hello World!".
    const replyBody = "Hello World!"; // we don't have any special styling to do.
    const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
    reply["msgtype"] = "m.notice";
    client.sendMessage(roomId, reply);
}
